package mx.edu.delasalle.calculadora

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import mx.edu.delasalle.calculadora.ui.theme.CalculadoraTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CalculadoraTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Greeting("Android")
                }
            }
        }
    }
}

@Composable
fun Body(){
    Scaffold(backgroundColor = Color.Black) {
        
        Text(
            text = "0",
            modifier = Modifier.fillMaxWidth(),
            textAlign = TextAlign.Right,
            fontSize = 80.sp,
            color = Color.White)
        
        Column(
                Modifier.fillMaxHeight(),
                verticalArrangement = Arrangement.Bottom,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {

            Row(Modifier.padding(3.dp)) {
                Button(onClick = { /*TODO*/ },
                    modifier = Modifier
                        .padding(1.dp)
                        .height(70.dp)
                        .width(95.dp),
                        colors = ButtonDefaults.buttonColors(backgroundColor = Color(0XFF074a19)),
                        shape = CircleShape) {

                    Text(text = "AC", color = Color.White)
                }
                Button(onClick = { /*TODO*/ },
                    modifier = Modifier
                        .padding(1.dp)
                        .height(70.dp)
                        .width(95.dp),
                        colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF316ec4)),
                        shape = CircleShape) {
                    Text(text = "()", color = Color.White)
                }
                Button(onClick = { /*TODO*/ },
                    modifier = Modifier
                        .padding(1.dp)
                        .height(70.dp)
                        .width(95.dp),
                        colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF316ec4)),
                        shape = CircleShape) {
                    Text(text = "%", color = Color.White)
                }
                Button(onClick = { /*TODO*/ },
                    modifier = Modifier
                        .padding(1.dp)
                        .height(70.dp)
                        .width(95.dp),
                        colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF316ec4)),
                        shape = CircleShape) {
                    Text(text = "÷", color = Color.White)
                }
            }
            Row(Modifier.padding(3.dp)) {
                Button(onClick = { /*TODO*/ },
                    modifier = Modifier
                        .padding(1.dp)
                        .height(70.dp)
                        .width(95.dp),
                        colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF373b38)),
                        shape = CircleShape)
                         {

                    Text(text = "7", color = Color.White)
                }
                Button(onClick = { /*TODO*/ },
                    modifier = Modifier
                        .padding(1.dp)
                        .height(70.dp)
                        .width(95.dp),
                        colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF373b38)),
                        shape = CircleShape) {
                    Text(text = "8", color = Color.White)
                }
                Button(onClick = { /*TODO*/ },
                    modifier = Modifier
                        .padding(1.dp)
                        .height(70.dp)
                        .width(95.dp),
                        shape = CircleShape,
                        colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF373b38))) {
                    Text(text = "9", color = Color.White)
                }
                Button(onClick = { /*TODO*/ },
                    modifier = Modifier
                        .padding(1.dp)
                        .height(70.dp)
                        .width(95.dp),
                        colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF316ec4)),
                        shape = CircleShape) {
                    Text(text = "x", color = Color.White)
                }
            }
            Row(Modifier.padding(3.dp)) {
                Button(onClick = { /*TODO*/ },
                    modifier = Modifier
                        .padding(1.dp)
                        .height(70.dp)
                        .width(95.dp),
                        shape = CircleShape,
                        colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF373b38))) {

                    Text(text = "4", color = Color.White)
                }
                Button(onClick = { /*TODO*/ },
                    modifier = Modifier
                        .padding(1.dp)
                        .height(70.dp)
                        .width(95.dp),
                        colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF373b38)),
                        shape = CircleShape) {
                    Text(text = "5", color = Color.White)
                }
                Button(onClick = { /*TODO*/ },
                    modifier = Modifier
                        .padding(1.dp)
                        .height(70.dp)
                        .width(95.dp),
                        colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF373b38)),
                        shape = CircleShape) {
                    Text(text = "6", color = Color.White)
                }
                Button(onClick = { /*TODO*/ },
                    modifier = Modifier
                        .padding(1.dp)
                        .height(70.dp)
                        .width(95.dp),
                        colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF316ec4)),
                        shape = CircleShape) {
                    Text(text = "-", color = Color.White)
                }
            }
            Row(Modifier.padding(3.dp)) {
                Button(onClick = { /*TODO*/ },
                        modifier = Modifier
                            .padding(1.dp)
                            .height(70.dp)
                            .width(95.dp),
                            colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF373b38)),
                            shape = CircleShape ) {

                    Text(text = "1", color = Color.White)
                }
                Button(onClick = { /*TODO*/ },
                        modifier = Modifier
                            .padding(1.dp)
                            .height(70.dp)
                            .width(95.dp),
                            shape = CircleShape,
                            colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF373b38))) {
                    Text(text = "2", color = Color.White)
                }
                Button(onClick = { /*TODO*/ },
                        modifier = Modifier
                            .padding(1.dp)
                            .height(70.dp)
                            .width(95.dp),
                            colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF373b38)),
                            shape = CircleShape) {
                    Text(text = "3", color = Color.White)
                }
                Button(onClick = { /*TODO*/ },
                        modifier = Modifier
                            .padding(1.dp)
                            .height(70.dp)
                            .width(95.dp),
                            colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF316ec4)),
                            shape = CircleShape) {
                    Text(text = "+", color = Color.White)
                }
            }
            Row(Modifier.padding(3.dp)) {
                Button(onClick = { /*TODO*/ },
                    modifier = Modifier
                        .padding(1.dp)
                        .height(70.dp)
                        .width(95.dp),
                        shape = CircleShape,
                        colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF373b38))) {

                    Text(text = "0", color = Color.White)
                }
                Button(onClick = { /*TODO*/ },
                    modifier = Modifier
                        .padding(1.dp)
                        .height(70.dp)
                        .width(95.dp),
                        shape = CircleShape,
                        colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF373b38))) {
                    Text(text = ".", color = Color.White)
                }
                Button(onClick = { /*TODO*/ },
                    modifier = Modifier
                        .padding(1.dp)
                        .height(70.dp)
                        .width(95.dp),
                        shape = CircleShape,
                        colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF373b38))) {
                    Text(text = "Delete", color = Color.White)
                }
                Button(onClick = { /*TODO*/ },
                    modifier = Modifier
                        .padding(1.dp)
                        .height(70.dp)
                        .width(95.dp),
                        colors = ButtonDefaults.buttonColors(backgroundColor = Color(0xFF2f34de)),
                        shape = CircleShape) {
                    Text(text = "=", color = Color.White)
                }
            }

        }


    }
    
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    CalculadoraTheme {
        //Greeting("Android")
        Body()
    }
}